#!/usr/bin/python3

import re,os,sys,subprocess

def load_images(path):
    "Load docker images from file"

    tagname_regex = re.compile(r'(?P<repo>\S+)_(?P<tag>\S+)_(?P<img_id>\S+)\.tar')

    for name in os.listdir(path):
        if not name.endswith('.tar'):
            continue
        full_path = os.path.join(path, name)
        print('Loading {0}'.format(full_path))
        subprocess.check_call(['docker', 'load', '--input', full_path])
        match = tagname_regex.match(name)
        if match:
            img_info = match.groupdict()
            if img_info['repo'] == '<none>' or img_info['tag'] == '<none>':
                continue
            # Place original tags back in image
            subprocess.check_call(['docker', 'tag', img_info['img_id'], img_info['repo'].replace('-', '/', 1) + ':' + img_info['tag']])

if __name__ == "__main__":
    if len(sys.argv) > 1:
        path = sys.argv[1]
    else:
        path = ''
    load_images(path)

#!/usr/bin/python3

import os,sys,subprocess

def docker_images():
    "Retrieve docker image info"
    fmt_string = '{{.Repository}}\t{{.Tag}}\t{{.ID}}'
    output = subprocess.check_output(['docker', 'images', '--format', fmt_string])
    return [line.decode('utf-8').split('\t', 2) for line in output.splitlines()]

def save_images(dest_path):
    "Save docker images to file"
    for (img_repo, img_tag, img_id) in docker_images():
        tarball = os.path.join(dest_path, '{repo}_{tag}_{img_id}.tar'.format(repo=img_repo.replace('/', '-'), tag=img_tag, img_id=img_id))
        print('Saving {repo}:{tag} to {tar}'.format(repo=img_repo, tag=img_tag, tar=tarball))
        subprocess.check_call(['docker', 'save', '--output', tarball, img_id])

if __name__ == "__main__":
    if len(sys.argv) > 1:
        path = sys.argv[1]
    else:
        path = ''
    save_images(path)
